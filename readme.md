[The Color API](//thecolorapi.com)
========

Your fast, modern, swiss army knife for color. 

Pass in any valid color and get conversion into any other format, the name of the color, placeholder images and a multitude of schemes.
